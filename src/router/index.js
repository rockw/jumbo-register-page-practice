import { createRouter, createWebHistory } from 'vue-router'
import { onMounted } from 'vue'
// import Home from '../views/Home.vue'

const routes = [
  {
    path: '/login-or-register/login',
    name: 'loginOrRegister',
    component: () => import('@/components/SectionLoginOrRegister.vue'),
    children: [
      {
        path: '/login-or-register/register',
        name: 'register',
        component: () => import('@/components/SectionRegister.vue')
      },
      {
        path: '/login-or-register/login',
        name: 'login',
        component: () => import('@/components/SectionLogin.vue'),
      }
    ]
  },
  {
    path: '/game',
    name: 'game',
    component: () => import('@/components/SectionGame.vue')
  },
  {
    path: '/promotion',
    name: 'promotion',
    component: () => import('@/components/SectionPromotion.vue')
  },
  {
    path: '/mine',
    name: 'mine',
    component: () => import('@/components/SectionMine.vue')
  },
  {
    path: '/helper',
    name: 'helper',
    component: () => import('@/components/SectionHelper.vue')
  },
  {
    path: '/proxy',
    name: 'proxy',
    component: () => import('@/components/SectionProxy.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

onMounted(() => {
  router.beforeEach((to, from) => {
    console.log('to', to)
    console.log('from', from)
  
    // document.querySelector('.menu').classList.add('no-show')
  })
})


export default router
